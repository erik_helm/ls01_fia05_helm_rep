﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
       double zuZahlenderBetrag = 0.0; 
       double eingezahlterGesamtbetrag = 0.0;
       

       zuZahlenderBetrag = fahrkartenbestellungErfassen(zuZahlenderBetrag);
       eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rückgeldAusgabe(eingezahlterGesamtbetrag,zuZahlenderBetrag);
     

    }
       
       
    public static double fahrkartenbestellungErfassen(double zuZahlenderBetrag)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	   double Anzahl;
    	   
    	  System.out.print("Zu zahlender Betrag (EURO): ");
          zuZahlenderBetrag = tastatur.nextDouble();
          System.out.print("Anzahl der Fahrkarten: ");
          Anzahl = tastatur.nextDouble();
          
          zuZahlenderBetrag = zuZahlenderBetrag * Anzahl;
          return zuZahlenderBetrag;
    }
    public static double fahrkartenBezahlen(double zuZahlenderBetrag)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
        double eingeworfeneMünze = 0.0;
        double zahl = 0.0;
        while(zahl < zuZahlenderBetrag)
        {
     	   System.out.print("Noch zu zahlen: ");
     	   System.out.printf("%.2f%n", (zuZahlenderBetrag - zahl));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   zahl += eingeworfeneMünze;
    }  
        return zahl;
    }
    public static void fahrkartenAusgeben()
    {
    	Scanner tastatur = new Scanner(System.in);
    	  
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
           
        }
       

    }
    public static void rückgeldAusgabe(double eingezahlterGesamtbetrag, double zuZahlenderBetrag)
    {
    	System.out.printf("\n\n");
    	double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("\nDer Rückgabebetrag in Höhe von %.2f%s%n" , rückgabebetrag , " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 1.99) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.00;
            }
            while(rückgabebetrag >= 0.99) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.49) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.19) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.09) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.");
     }
    }